with open("input.txt", mode="r") as f:
    ftext = f.readlines()

is_list = []
m = int(ftext[-1])

for j in ftext[:-1]:
	i = int(j.split(":")[0])
	if m % i == 0:
		is_list.append(j.rstrip())

if is_list == []:
	print(m)
else:
	is_list.sort(key=lambda x: int(x.split(":")[0]))
	is_list = "".join([j.split(":")[1] for j in is_list])
	print(is_list)